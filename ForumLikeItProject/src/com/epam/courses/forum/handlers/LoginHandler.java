package com.epam.courses.forum.handlers;
/**
 * LoginHandler is a class that provide common methods
 * for login action.
 *
 * @author Murdeus
 * @version 1.0
 * @since 1.0
 */
import com.epam.courses.forum.connectionpool.ConnectionPool;
import com.epam.courses.forum.dao.UserDao;
import com.epam.courses.forum.entity.User;
import com.epam.courses.forum.exceptions.RequestOutOfRangeException;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;
import java.sql.SQLException;

public class LoginHandler {

    private static final Logger logger = Logger.getLogger(LoginHandler.class);
    private User loggedUser;

    /**
     * Method for validation user's info.
     *
     * @param request Request with parameters.
     * @return result of validation.
     */
    public int validate(HttpServletRequest request) {
        int result = 0;
        try {
            User user = new User();
            user.setLogin(request.getParameter("inputUsername"));
            user.setPassword(request.getParameter("inputPassword"));
            Connection connection = ConnectionPool.getConnection();
            UserDao userDao = new UserDao(connection);
            if (userDao.checkIfExist(user) == 1) {
                loggedUser = userDao.getEntityById(userDao.getUserSessionId(user));
                result = 1;
            }
            ConnectionPool.closeConnection(connection);
        } catch (SQLException | RequestOutOfRangeException ex) {
            logger.warn(ex.toString());
        }
        return result;
    }

    public User getLoggedUser() {
        return loggedUser;
    }
}
