package com.epam.courses.forum.handlers;
/**
 * PostHandler is a class that provide common methods
 * for post manipulation.
 *
 * @author Murdeus
 * @version 1.0
 * @since 1.0
 */
import com.epam.courses.forum.connectionpool.ConnectionPool;
import com.epam.courses.forum.dao.CommentDao;
import com.epam.courses.forum.dao.PostDao;
import com.epam.courses.forum.dao.UserDao;
import com.epam.courses.forum.entity.Post;
import com.epam.courses.forum.exceptions.RequestOutOfRangeException;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PostHandler {

    private static final Logger logger = Logger.getLogger(PostHandler.class);
    private static final String TOPIC_VALIDATE_STRING = ".{1,128}";
    private static final String TEXT_VALIDATE_STRING = "([\\s\\S]){1,512}";

    /**
     * Method for validation post body and topic.
     *
     * @param topic Post topic text.
     * @param text Post body text.
     * @return result of validation.
     */
    public Boolean validate(String topic, String text) {
        if (topic.trim().isEmpty() || text.trim().isEmpty()) {
            return false;
        }
        Pattern pattern = Pattern.compile(TOPIC_VALIDATE_STRING);
        Matcher matcher = pattern.matcher(topic);
        if (!matcher.matches()) {
            return false;
        }
        pattern = Pattern.compile(TEXT_VALIDATE_STRING);
        matcher = pattern.matcher(text);
        if (!matcher.matches()) {
            return false;
        }
        return true;
    }

    /**
     * Method that puts post into DB.
     *
     * @param request Request with parameters.
     * @return result of insert.
     */
    public Boolean submitPost(HttpServletRequest request) {
        Boolean result = false;
        try {
            Connection connection = ConnectionPool.getConnection();
            PostDao postDao = new PostDao(connection);
            Post post = new Post();
            post.setMessageTopic(request.getParameter("inputPostTopic"));
            post.setMessageText(request.getParameter("inputPostBody"));
            post.setMessageDate(new Date());
            post.setUserId((Integer) request.getSession().getAttribute("userId"));
            if (postDao.create(post) == 1) {
                result = true;
            }
            ConnectionPool.closeConnection(connection);
        } catch (SQLException ex) {
            logger.warn(ex.toString());
        }
        return result;
    }

    /**
     * Method for post update.
     *
     * @param id Id of the comment.
     * @param text Updated post body text.
     * @param topic Updated post topic text.
     * @return result of update.
     */
    public Boolean updatePost(int id, String text, String topic) {
        Boolean result = false;
        try {
            Connection connection = ConnectionPool.getConnection();
            PostDao postDao = new PostDao(connection);
            Post post = new Post();
            post.setId(id);
            post.setMessageText(text);
            post.setMessageTopic(topic);
            if (postDao.update(post) == 1) {
                result = true;
            }
            ConnectionPool.closeConnection(connection);
        } catch (SQLException ex) {
            logger.warn(ex.toString());
        }
        return result;
    }

    /**
     * Method that retrives comment from DB.
     *
     * @param postId Id of requested post.
     * @return comment Comment object.
     * @throws RequestOutOfRangeException If post doesn't
     *                                    exist in DB.
     */
    public Post getPost(int postId) throws RequestOutOfRangeException {
        Post post = null;
        try {
            Connection connection = ConnectionPool.getConnection();
            PostDao postDao = new PostDao(connection);
            UserDao userDao = new UserDao(connection);
            post = postDao.getEntityById(postId);
            post.setUserName(userDao.getEntityById(post.getUserId()).getLogin());
            ConnectionPool.closeConnection(connection);
        } catch (SQLException ex) {
            logger.warn(ex.toString());
        }
        return post;
    }

    /**
     * Method that deletes post from DB.
     *
     * @param postId Id of requested post.
     * @return result Result of deleting.
     */
    public Boolean deletePost(int postId) {
        Boolean result = false;
        try {
            Connection connection = ConnectionPool.getConnection();
            PostDao postDao = new PostDao(connection);
            if (postDao.deleteEntityById(postId) == 1) {
                result = true;
            }
            ConnectionPool.closeConnection(connection);
        } catch (SQLException ex) {
            logger.warn(ex.toString());
        }
        return result;
    }

    /**
     * Method that gets list of post from DB.
     *
     * @return posts Posts list.
     */
    public List<Post> getPostList() {
        List<Post> posts = null;
        try {
            Connection connection = ConnectionPool.getConnection();
            PostDao postDao = new PostDao(connection);
            UserDao userDao = new UserDao(connection);
            CommentDao commentDao = new CommentDao(connection);
            posts = postDao.getAll(0);
            for (Post post : posts) {
                post.setUserName(userDao.getEntityById(post.getUserId()).getLogin());
                post.setCommentNumber(commentDao.getAll(post.getId()).size());
            }
            ConnectionPool.closeConnection(connection);
        } catch (SQLException | RequestOutOfRangeException ex) {
            logger.warn(ex.toString());
        }
        return posts;
    }

    /**
     * Method that gets list of post from DB.
     *
     * @param userId id of related user.
     * @return posts Posts list.
     */
    public List<Post> getRelatedPostList(int userId) {
        List<Post> posts = null;
        try {
            Connection connection = ConnectionPool.getConnection();
            PostDao postDao = new PostDao(connection);
            UserDao userDao = new UserDao(connection);
            CommentDao commentDao = new CommentDao(connection);
            posts = postDao.getAll(userId);
            for (Post post : posts) {
                post.setUserName(userDao.getEntityById(post.getUserId()).getLogin());
                post.setCommentNumber(commentDao.getAll(post.getId()).size());
            }
            ConnectionPool.closeConnection(connection);
        } catch (SQLException | RequestOutOfRangeException ex) {
            logger.warn(ex.toString());
        }
        return posts;
    }
}
