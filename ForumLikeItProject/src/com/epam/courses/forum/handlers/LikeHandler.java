package com.epam.courses.forum.handlers;
/**
 * LikeHandler is a class that provide common methods
 * for rating actions.
 *
 * @author Murdeus
 * @version 1.0
 * @since 1.0
 */
import com.epam.courses.forum.connectionpool.ConnectionPool;
import com.epam.courses.forum.dao.CommentDao;
import com.epam.courses.forum.dao.RatingDao;
import com.epam.courses.forum.entity.Rating;
import com.epam.courses.forum.exceptions.RequestOutOfRangeException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;

public class LikeHandler {

    private int commentRating;

    /**
     * Method that inserts rating to DB.
     *
     * @param commentId Id of related comment.
     * @param userId Id of user that inserts comment.
     * @throws SQLException If a database access
     *                      error occurs
     * @throws RequestOutOfRangeException If any object doesn't
     *                                    exist in DB.
     */
    public void insertRating(int commentId, int userId) throws SQLException, RequestOutOfRangeException {
        Rating newRating = new Rating();
        newRating.setCommentId(commentId);
        newRating.setUserId(userId);
        newRating.setRatingDate(new Date());
        Connection connection = ConnectionPool.getConnection();
        RatingDao ratingDao = new RatingDao(connection);
        CommentDao commentDao = new CommentDao(connection);
        Rating oldRating = ratingDao.getRatingInfo(commentId, userId);
        connection.setAutoCommit(false);
        if (oldRating == null) {
            newRating.setRating(1);
            ratingDao.create(newRating);
        } else {
            if (oldRating.getRating() == 1) {
                newRating.setRating(-1);
            } else {
                newRating.setRating(1);
            }
            ratingDao.update(newRating);
        }
        commentRating = commentDao.getEntityById(commentId).getRating();
        connection.commit();
        ConnectionPool.closeConnection(connection);
    }

    public int getCommentRating() {
        return commentRating;
    }
}
