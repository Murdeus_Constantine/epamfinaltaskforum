package com.epam.courses.forum.handlers;
/**
 * UserHandler is a class that provide common methods
 * for post manipulation.
 *
 * @author Murdeus
 * @version 1.0
 * @since 1.0
 */
import com.epam.courses.forum.connectionpool.ConnectionPool;
import com.epam.courses.forum.dao.UserDao;
import com.epam.courses.forum.entity.User;
import com.epam.courses.forum.exceptions.RequestOutOfRangeException;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class UserHandler {

    private static final Logger logger = Logger.getLogger(UserHandler.class);
    private Boolean userState;

    /**
     * Method that deletes user from DB.
     *
     * @param userId Id of requested user.
     * @return result Result of deleting.
     */
    public Boolean deleteUser(int userId) {
        Boolean result = false;
        try {
            Connection connection = ConnectionPool.getConnection();
            UserDao userDao = new UserDao(connection);
            if (userDao.deleteEntityById(userId) == 1) {
                result = true;
            }
            ConnectionPool.closeConnection(connection);
        } catch (SQLException ex) {
            logger.warn(ex.toString());
        }
        return result;
    }

    /**
     * Method that gets list of users from DB.
     *
     * @return users Users list.
     */
    public List<User> getUserList() {
        List<User> users = null;
        try {
            Connection connection = ConnectionPool.getConnection();
            UserDao userDao = new UserDao(connection);
            users = userDao.getAll(0);
            ConnectionPool.closeConnection(connection);
        } catch (SQLException ex) {
            logger.warn(ex.toString());
        }
        return users;
    }

    /**
     * Method that retrives user from DB.
     *
     * @param userId Id of requested user.
     * @return user User object.
     * @throws RequestOutOfRangeException If user doesn't
     *                                    exist in DB.
     */
    public User getUser(int userId) throws RequestOutOfRangeException {
        User user = null;
        try {
            Connection connection = ConnectionPool.getConnection();
            UserDao userDao = new UserDao(connection);
            user = userDao.getEntityById(userId);
            ConnectionPool.closeConnection(connection);
        } catch (SQLException ex) {
            logger.warn(ex.toString());
        }
        return user;
    }

    /**
     * Method that changes current user's state.
     *
     * @param id Id of user.
     * @return result of update.
     * @throws SQLException If a database access
     *                      error occurs
     * @throws RequestOutOfRangeException If user doesn't
     *                                    exist in DB.
     */
    public void changeUserState(int id) throws SQLException, RequestOutOfRangeException{
        User newUser = new User();
        newUser.setId(id);
        Connection connection = ConnectionPool.getConnection();
        UserDao userDao = new UserDao(connection);
        User oldUser = userDao.getEntityById(id);
        connection.setAutoCommit(false);
        if (oldUser.getActive()) {
            newUser.setActive(false);
        } else {
            newUser.setActive(true);
        }
        userDao.update(newUser);
        userState = userDao.getEntityById(id).getActive();
        connection.commit();
        ConnectionPool.closeConnection(connection);
    }

    public Boolean getUserState() {
        return userState;
    }
}
