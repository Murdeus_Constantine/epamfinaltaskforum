package com.epam.courses.forum.handlers;
/**
 * CommentHandler is a class that provide common methods
 * for comment manipulation.
 *
 * @author Murdeus
 * @version 1.0
 * @since 1.0
 */
import com.epam.courses.forum.connectionpool.ConnectionPool;
import com.epam.courses.forum.dao.CommentDao;
import com.epam.courses.forum.dao.UserDao;
import com.epam.courses.forum.entity.Comment;
import com.epam.courses.forum.exceptions.RequestOutOfRangeException;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommentHandler {

    private static final Logger logger = Logger.getLogger(CommentHandler.class);
    private static final String VALIDATE_STRING = "([\\s\\S]){1,128}";

    /**
     * Method for validation comment body.
     *
     * @param text Comment body text.
     * @return result of validation.
     */
    public Boolean validate(String text) {
        if (text.trim().isEmpty()) {
            return false;
        }
        Pattern pattern = Pattern.compile(VALIDATE_STRING);
        Matcher matcher = pattern.matcher(text);
        if (!matcher.matches()) {
            return false;
        }
        return true;
    }

    /**
     * Method for comment update.
     *
     * @param id Id of the comment.
     * @param text Updated comment body text.
     * @return result of update.
     */
    public Boolean updateComment(int id, String text) {
        Boolean result = false;
        try {
            Connection connection = ConnectionPool.getConnection();
            CommentDao commentDao = new CommentDao(connection);
            Comment comment = new Comment();
            comment.setId(id);
            comment.setCommentText(text);
            if (commentDao.update(comment) == 1) {
                result = true;
            }
            ConnectionPool.closeConnection(connection);
        } catch (SQLException ex) {
            logger.warn(ex.toString());
        }
        return result;
    }

    /**
     * Method that puts comment into DB.
     *
     * @param request Request with parameters.
     * @return result of insert.
     */
    public Boolean submitComment(HttpServletRequest request) {
        Boolean result = false;
        try {
            Connection connection = ConnectionPool.getConnection();
            CommentDao commentDao = new CommentDao(connection);
            Comment comment = new Comment();
            comment.setCommentText(request.getParameter("inputComment"));
            comment.setMessageId(Integer.parseInt(request.getParameter("postId")));
            comment.setCommentDate(new Date());
            comment.setUserId((Integer) request.getSession().getAttribute("userId"));
            if (commentDao.create(comment) == 1) {
                result = true;
            }
            ConnectionPool.closeConnection(connection);
        } catch (SQLException ex) {
            logger.warn(ex.toString());
        }
        return result;
    }

    /**
     * Method that retrives comment from DB.
     *
     * @param commentId Id of requested comment.
     * @return comment Comment object.
     * @throws RequestOutOfRangeException If comment doesn't
     *                                    exist in DB.
     */
    public Comment getComment(int commentId) throws RequestOutOfRangeException {
        Comment comment = null;
        try {
            Connection connection = ConnectionPool.getConnection();
            CommentDao commentDao = new CommentDao(connection);
            comment = commentDao.getEntityById(commentId);
            ConnectionPool.closeConnection(connection);
        } catch (SQLException ex) {
            logger.warn(ex.toString());
        }
        return comment;
    }

    /**
     * Method that deletes comment from DB.
     *
     * @param commentId Id of requested comment.
     * @return result Result of deleting.
     */
    public Boolean deleteComment(int commentId) {
        Boolean result = false;
        try {
            Connection connection = ConnectionPool.getConnection();
            CommentDao commentDao = new CommentDao(connection);
            if (commentDao.deleteEntityById(commentId) == 1) {
                result = true;
            }
            ConnectionPool.closeConnection(connection);
        } catch (SQLException ex) {
            logger.warn(ex.toString());
        }
        return result;
    }

    /**
     * Method that gets list of comment to concrete post
     * from DB.
     *
     * @param postId Id of related post.
     * @return comments Comments list.
     */
    public List<Comment> getCommentList(int postId) {
        List<Comment> comments = null;
        try {
            Connection connection = ConnectionPool.getConnection();
            CommentDao commentDao = new CommentDao(connection);
            UserDao userDao = new UserDao(connection);
            comments = commentDao.getAll(postId);
            for (Comment comment : comments){
                comment.setUserName(userDao.getEntityById(comment.getUserId()).getLogin());
            }
            ConnectionPool.closeConnection(connection);
        } catch (SQLException | RequestOutOfRangeException ex) {
            logger.warn(ex.toString());
        }
        return comments;
    }
}
