package com.epam.courses.forum.handlers;
/**
 * RegistrationHandler is a class that provide methods
 * for registration action.
 *
 * @author Murdeus
 * @version 1.0
 * @since 1.0
 */
import com.epam.courses.forum.connectionpool.ConnectionPool;
import com.epam.courses.forum.dao.UserDao;
import com.epam.courses.forum.entity.User;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegistrationHandler {

    private static final Logger logger = Logger.getLogger(RegistrationHandler.class);
    private static final String VALIDATE_EMAIL = ".+@.+\\..+";

    /**
     * Method for validation user's info.
     *
     * @param request Request with parameters.
     * @return result of validation.
     */
    public Boolean validate(HttpServletRequest request) {
        if (request.getParameter("inputUsername").trim().isEmpty() ||request.getParameter("inputPassword").trim().isEmpty()) {
            return false;
        }
        Boolean result = false;
        if (!request.getParameter("inputPassword").equals(request.getParameter("inputPasswordConfirm"))) {
            return false;
        }
        Pattern pattern = Pattern.compile(VALIDATE_EMAIL);
        Matcher matcher = pattern.matcher(request.getParameter("inputEmail"));
        if (!matcher.matches()) {
            return false;
        }
        try {
            Connection connection = ConnectionPool.getConnection();
            UserDao userDao = new UserDao(connection);
            User user = new User();
            user.setLogin(request.getParameter("inputUsername"));
            user.setPassword(request.getParameter("inputPassword"));
            user.setEmail(request.getParameter("inputEmail"));
            user.setFullname(request.getParameter("inputSurname") + " " + request.getParameter("inputName") + " "
                    + request.getParameter("inputFathername"));
            if (userDao.checkIfEnable(user) == 0) {
                result = true;
            }
            ConnectionPool.closeConnection(connection);
        } catch (SQLException ex) {
            logger.warn(ex.toString());
        }
        return result;
    }

    /**
     * Method that puts user into DB.
     *
     * @param request Request with parameters.
     * @return result of creation.
     */
    public Boolean register(HttpServletRequest request) {
        Boolean result = false;
        try {
            Connection connection = ConnectionPool.getConnection();
            UserDao userDao = new UserDao(connection);
            User user = new User();
            user.setLogin(request.getParameter("inputUsername"));
            user.setPassword(request.getParameter("inputPassword"));
            user.setEmail(request.getParameter("inputEmail"));
            user.setFullname(request.getParameter("inputSurname") + " " + request.getParameter("inputName") + " "
                    + request.getParameter("inputFathername"));
            if (userDao.create(user) == 1) {
                result = true;
            }
            ConnectionPool.closeConnection(connection);
        } catch (SQLException ex) {
            logger.warn(ex.toString());
        }
        return result;
    }
}
