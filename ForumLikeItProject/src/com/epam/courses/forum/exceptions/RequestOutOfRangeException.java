package com.epam.courses.forum.exceptions;
/**
 * RequestOutOfRangeException is a exception class,
 * throws in situations where requested parameter
 * do not exists in DB.
 *
 * @author Murdeus
 * @version 1.0
 * @since 1.0
 */
public class RequestOutOfRangeException extends Exception {

    /**
     * Constructs a new exception with {@code null} as its detail message.
     */
    public RequestOutOfRangeException() {
        super();
    }

    /**
     * Constructs a new exception with the specified detail message.
     *
     * @param   message   the detail message. The detail message is saved for
     *          later retrieval by the {@link #getMessage()} method.
     */
    public RequestOutOfRangeException(String message) {
        super(message);
    }

    /**
     * Constructs a new exception with the specified cause and a detail
     * message of <tt>(cause==null ? null : cause.toString())</tt> (which
     * typically contains the class and detail message of <tt>cause</tt>).
     *
     * @param  cause the cause (which is saved for later retrieval by the
     *         {@link #getCause()} method).  (A <tt>null</tt> value is
     *         permitted, and indicates that the cause is nonexistent or
     *         unknown.)
     * @since  1.4
     */
    public RequestOutOfRangeException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructs a new exception with the specified detail message and
     * cause.
     *
     * @param  message the detail message (which is saved for later retrieval
     *         by the {@link #getMessage()} method).
     * @param  cause the cause (which is saved for later retrieval by the
     *         {@link #getCause()} method).
     * @since  1.4
     */
    public RequestOutOfRangeException(String message, Throwable cause) {
        super(message, cause);
    }
}
