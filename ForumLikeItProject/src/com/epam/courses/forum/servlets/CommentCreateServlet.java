package com.epam.courses.forum.servlets;
/**
 * CommentCreateServlet is a class that provide an opportunity
 * to create comments.
 *
 * @author Murdeus
 * @version 1.0
 * @see javax.servlet.http.HttpServlet
 * @since 1.0
 */
import com.epam.courses.forum.handlers.CommentHandler;
import org.apache.log4j.Logger;
import com.epam.courses.forum.resourcemanager.MessageManager;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/post/commentsubmit")
public class CommentCreateServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(CommentCreateServlet.class);

    /**
     * Method that reacts on GET http request and send it on processing
     *
     * @param request Incoming request
     * @param response Object to assist a servlet in sending a
     *                 response to the client.
     * @throws IOException If a I/O error occurs
     * @throws ServletException If an error with servlet occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Method that reacts on POST http request and send it on processing
     *
     * @param request Incoming request
     * @param response Object to assist a servlet in sending a
     *                 response to the client.
     * @throws IOException If a I/O error occurs
     * @throws ServletException If an error with servlet occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Method that process a comment creation and redirects user
     * depends on operation success.
     *
     * @param request Incoming request
     * @param response Object to assist a servlet in sending a
     *                 response to the client.
     * @throws IOException If a I/O error occurs
     * @throws ServletException If an error with servlet occurs
     */
    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        if (request.getParameter("command") != null) {
            CommentHandler commentHandler = new CommentHandler();
            if (commentHandler.validate(request.getParameter("inputComment"))) {
                if (commentHandler.submitComment(request)) {
                    response.sendRedirect("/post/" + request.getParameter("postId"));
                } else {
                    logger.warn("Database error while creating comment.");
                }
            } else {
                logger.info("Incorrect input in comment fields.");
                request.setAttribute("errorCommentCreationMessage", MessageManager.getProperty("message.comment.create.error"));
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(
                        "/post/" + request.getParameter("postId"));
                dispatcher.forward(request, response);
            }
        } else {
            logger.info("Empty command in comment create request.");
            response.sendRedirect("/post/" + request.getParameter("postId"));
        }
    }
}
