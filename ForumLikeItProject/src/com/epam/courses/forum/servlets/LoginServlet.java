package com.epam.courses.forum.servlets;
/**
 * LoginServlet is a class that provide an opportunity for logging.
 *
 * @author Murdeus
 * @version 1.0
 * @see javax.servlet.http.HttpServlet
 * @since 1.0
 */
import com.epam.courses.forum.handlers.LoginHandler;
import org.apache.log4j.Logger;
import com.epam.courses.forum.resourcemanager.ConfigurationManager;
import com.epam.courses.forum.resourcemanager.MessageManager;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(LoginServlet.class);

    /**
     * Method that reacts on GET http request and send it on processing
     *
     * @param request Incoming request
     * @param response Object to assist a servlet in sending a
     *                 response to the client.
     * @throws IOException If a I/O error occurs
     * @throws ServletException If an error with servlet occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("Login command is not reachable in GET request.");
        response.sendRedirect(ConfigurationManager.getProperty("path.page.index"));
    }

    /**
     * Method that reacts on POST http request and send it on processing
     *
     * @param request Incoming request
     * @param response Object to assist a servlet in sending a
     *                 response to the client.
     * @throws IOException If a I/O error occurs
     * @throws ServletException If an error with servlet occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Method that process a login operation.
     *
     * @param request Incoming request
     * @param response Object to assist a servlet in sending a
     *                 response to the client.
     * @throws IOException If a I/O error occurs
     * @throws ServletException If an error with servlet occurs
     */
    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        if (request.getParameter("command") != null) {
            LoginHandler loginHandler = new LoginHandler();
            if (loginHandler.validate(request) == 1) {
                if(loginHandler.getLoggedUser().getActive()) {
                    HttpSession session = request.getSession(true);
                    session.setAttribute("userId", loginHandler.getLoggedUser().getId());
                    session.setAttribute("userRole", loginHandler.getLoggedUser().getRole());
                    session.setAttribute("userLogin", request.getParameter("inputUsername"));
                    response.sendRedirect(ConfigurationManager.getProperty("path.page.index"));
                    logger.info("Successful log in.");
                } else {
                    logger.info("Deny attempt to log in while banned.");
                    request.setAttribute("errorLoginBannedMessage", MessageManager.getProperty("message.login.banned"));
                    RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(
                            ConfigurationManager.getProperty("path.page.login"));
                    dispatcher.forward(request, response);
                }
            } else {
                logger.info("Incorrect input in login form or there is no such user.");
                request.setAttribute("errorLoginPassMessage", MessageManager.getProperty("message.login.error"));
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(
                        ConfigurationManager.getProperty("path.page.login"));
                dispatcher.forward(request, response);
            }
        } else {
            logger.info("Empty command in login request.");
            response.sendRedirect(ConfigurationManager.getProperty("path.page.index"));
        }
    }

}
