package com.epam.courses.forum.servlets;
/**
 * CommentEditServlet is a class that provide an opportunity
 * to edit comment for editing.
 *
 * @author Murdeus
 * @version 1.0
 * @see javax.servlet.http.HttpServlet
 * @since 1.0
 */
import com.epam.courses.forum.handlers.CommentHandler;
import org.apache.log4j.Logger;
import com.epam.courses.forum.resourcemanager.MessageManager;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/preparecomment/commentedit")
public class CommentEditServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(CommentEditServlet.class);

    /**
     * Method that reacts on GET http request and send it on processing
     *
     * @param request Incoming request
     * @param response Object to assist a servlet in sending a
     *                 response to the client.
     * @throws IOException If a I/O error occurs
     * @throws ServletException If an error with servlet occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("Edit command is not reachable in GET request.");
        response.sendRedirect("/post/" + request.getParameter("postId"));
    }

    /**
     * Method that reacts on POST http request and send it on processing
     *
     * @param request Incoming request
     * @param response Object to assist a servlet in sending a
     *                 response to the client.
     * @throws IOException If a I/O error occurs
     * @throws ServletException If an error with servlet occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Method that process a comment edition operation and redirects user.
     *
     * @param request Incoming request
     * @param response Object to assist a servlet in sending a
     *                 response to the client.
     * @throws IOException If a I/O error occurs
     * @throws ServletException If an error with servlet occurs
     */
    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        if (request.getParameter("command") != null) {
            CommentHandler commentHandler = new CommentHandler();
            if (commentHandler.validate(request.getParameter("inputCommentEdit"))) {
                if (commentHandler.updateComment(Integer.parseInt(request.getParameter("commentEditId")),
                        request.getParameter("inputCommentEdit"))) {
                    response.sendRedirect("/post/" + request.getParameter("postId"));
                } else {
                    logger.warn("Database error while updating comment.");
                }
            } else {
                logger.info("Incorrect input in comment fields while changing it.");
                request.setAttribute("errorCommentEditMessage", MessageManager.getProperty("message.comment.edit.error"));
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(
                        "/post/" + request.getParameter("postId"));
                dispatcher.forward(request, response);
            }
        } else {
            logger.info("Empty command.");
            response.sendRedirect("/post/" + request.getParameter("postId"));
        }
    }
}
