package com.epam.courses.forum.servlets;
/**
 * MyQuestionsServlet is a class that loads posts of current user.
 *
 * @author Murdeus
 * @version 1.0
 * @see javax.servlet.http.HttpServlet
 * @since 1.0
 */
import com.epam.courses.forum.handlers.PostHandler;
import com.epam.courses.forum.resourcemanager.ConfigurationManager;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/myposts")
public class MyQuestionsServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(MyQuestionsServlet.class);

    /**
     * Method that reacts on GET http request and send it on processing
     *
     * @param request Incoming request
     * @param response Object to assist a servlet in sending a
     *                 response to the client.
     * @throws IOException If a I/O error occurs
     * @throws ServletException If an error with servlet occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Method that reacts on POST http request and send it on processing.
     *
     * @param request Incoming request
     * @param response Object to assist a servlet in sending a
     *                 response to the client.
     * @throws IOException If a I/O error occurs
     * @throws ServletException If an error with servlet occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Method that sets up page by getting posts of current user.
     *
     * @param request Incoming request
     * @param response Object to assist a servlet in sending a
     *                 response to the client.
     * @throws IOException If a I/O error occurs
     * @throws ServletException If an error with servlet occurs
     */
    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        if((request.getSession().getAttribute("userId") != null)) {
            PostHandler postHandler = new PostHandler();
            request.setAttribute("list", postHandler.getRelatedPostList((Integer) request.getSession().getAttribute("userId")));
            request.getRequestDispatcher(ConfigurationManager.getProperty("path.page.my.post")).forward(request, response);
        }else {
            logger.info("Deny attempt to reach user post page while not log in.");
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(
                    ConfigurationManager.getProperty("path.page.access.deny"));
            dispatcher.forward(request, response);
        }
    }
}
