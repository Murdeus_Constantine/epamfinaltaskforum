package com.epam.courses.forum.servlets;
/**
 * PostPageServlet is a class that loads post and comments
 * related to it.
 *
 * @author Murdeus
 * @version 1.0
 * @see javax.servlet.http.HttpServlet
 * @since 1.0
 */
import com.epam.courses.forum.entity.Comment;
import com.epam.courses.forum.entity.Post;
import com.epam.courses.forum.exceptions.RequestOutOfRangeException;
import com.epam.courses.forum.handlers.CommentHandler;
import com.epam.courses.forum.handlers.PostHandler;
import org.apache.log4j.Logger;
import com.epam.courses.forum.resourcemanager.ConfigurationManager;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/post/*")
public class PostPageServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(PostPageServlet.class);

    /**
     * Method that reacts on GET http request and send it on processing
     *
     * @param request Incoming request
     * @param response Object to assist a servlet in sending a
     *                 response to the client.
     * @throws IOException If a I/O error occurs
     * @throws ServletException If an error with servlet occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Method that reacts on POST http request and send it on processing
     *
     * @param request Incoming request
     * @param response Object to assist a servlet in sending a
     *                 response to the client.
     * @throws IOException If a I/O error occurs
     * @throws ServletException If an error with servlet occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Method that process load post and related comments.
     *
     * @param request Incoming request
     * @param response Object to assist a servlet in sending a
     *                 response to the client.
     * @throws IOException If a I/O error occurs
     * @throws ServletException If an error with servlet occurs
     */
    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String pathInfo = request.getPathInfo().replaceAll("/", "");
        try {
            int postId = Integer.parseInt(pathInfo);
            PostHandler postHandler = new PostHandler();
            Post post = postHandler.getPost(postId);
            CommentHandler commentHandler = new CommentHandler();
            List<Comment> comments = commentHandler.getCommentList(postId);
            request.setAttribute("post", post);
            request.setAttribute("postId", post.getId());
            request.setAttribute("list", comments);
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(
                    ConfigurationManager.getProperty("path.page.post.page"));
            dispatcher.forward(request, response);
        } catch (NumberFormatException | RequestOutOfRangeException ex) {
            logger.info("404. Requested URI is not found.");
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(
                    ConfigurationManager.getProperty("path.page.404"));
            dispatcher.forward(request, response);
        } catch (Exception ex) {
            logger.error("Error while loading post page. " + ex.toString());
        }
    }
}
