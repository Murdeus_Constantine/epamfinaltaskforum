package com.epam.courses.forum.servlets;
/**
 * CommentEditPrepareServlet is a class that provide an opportunity
 * to prepare comment for editing. However it's not response for directly
 * edit them.
 *
 * @author Murdeus
 * @version 1.0
 * @see javax.servlet.http.HttpServlet
 * @since 1.0
 */
import com.epam.courses.forum.exceptions.RequestOutOfRangeException;
import com.epam.courses.forum.handlers.LikeHandler;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/addLike")
public class LikeServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(LikeServlet.class);

    /**
     * Method that reacts on GET http request and send it on processing
     *
     * @param request Incoming request
     * @param response Object to assist a servlet in sending a
     *                 response to the client.
     * @throws IOException If a I/O error occurs
     * @throws ServletException If an error with servlet occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Method that reacts on POST http request and send it on processing
     *
     * @param request Incoming request
     * @param response Object to assist a servlet in sending a
     *                 response to the client.
     * @throws IOException If a I/O error occurs
     * @throws ServletException If an error with servlet occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Method that process like operation. If user not logged
     * then writes fail to output, so rating will not count.
     *
     * @param request Incoming request
     * @param response Object to assist a servlet in sending a
     *                 response to the client.
     * @throws IOException If a I/O error occurs
     * @throws ServletException If an error with servlet occurs
     */
    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        try {
            int idComment = Integer.parseInt(request.getParameter("idComment"));
            LikeHandler likeHandler = new LikeHandler();
            response.setContentType("text/plain");
            response.setCharacterEncoding("UTF-8");
            if (request.getSession().getAttribute("userId") != null) {
                likeHandler.insertRating(idComment, (Integer) request.getSession().getAttribute("userId"));
                response.getWriter().write("" + likeHandler.getCommentRating());
            } else {
                response.getWriter().write("fail");
            }
        } catch (SQLException ex) {
            logger.error("Error with like action. " + ex.toString());
        } catch (NumberFormatException | RequestOutOfRangeException ex) {
            logger.info("404. Requested URI is not found.");
        }
    }
}
