package com.epam.courses.forum.servlets;
/**
 * AdminServlet is a class that provide user an admin page
 * if this user have necessary permissions.
 *
 * @author Murdeus
 * @version 1.0
 * @see javax.servlet.http.HttpServlet
 * @since 1.0
 */
import com.epam.courses.forum.handlers.UserHandler;
import com.epam.courses.forum.resourcemanager.ConfigurationManager;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/admin")
public class AdminServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(AdminServlet.class);

    /**
     * Method that reacts on GET http request and send it on processing
     *
     * @param request Incoming request
     * @param response Object to assist a servlet in sending a
     *                 response to the client.
     * @throws IOException If a I/O error occurs
     * @throws ServletException If an error with servlet occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Method that reacts on POST http request and send it on processing
     *
     * @param request Incoming request
     * @param response Object to assist a servlet in sending a
     *                 response to the client.
     * @throws IOException If a I/O error occurs
     * @throws ServletException If an error with servlet occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Method that checks user role and, if this role is admin,
     * provides an access to admin page, where user can manipulate
     * other users.
     *
     * @param request Incoming request
     * @param response Object to assist a servlet in sending a
     *                 response to the client.
     * @throws IOException If a I/O error occurs
     * @throws ServletException If an error with servlet occurs
     */
    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        if("admin".equals(request.getSession().getAttribute("userRole"))) {
            UserHandler userHandler = new UserHandler();
            request.setAttribute("list", userHandler.getUserList());
            request.getRequestDispatcher(ConfigurationManager.getProperty("path.page.admin.page")).forward(request, response);
        } else {
            logger.info("Deny attempt to reach admin page.");
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(
                    ConfigurationManager.getProperty("path.page.access.deny"));
            dispatcher.forward(request, response);
        }
    }
}
