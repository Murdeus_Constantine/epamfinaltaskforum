package com.epam.courses.forum.servlets;
/**
 * UserServlet is a class that responsible ofr user profile action.
 *
 * @author Murdeus
 * @version 1.0
 * @see javax.servlet.http.HttpServlet
 * @since 1.0
 */
import com.epam.courses.forum.entity.User;
import com.epam.courses.forum.exceptions.RequestOutOfRangeException;
import com.epam.courses.forum.handlers.UserHandler;
import org.apache.log4j.Logger;
import com.epam.courses.forum.resourcemanager.ConfigurationManager;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/user/*")
public class UserServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(UserServlet.class);

    /**
     * Method that reacts on GET http request and send it on processing
     *
     * @param request Incoming request
     * @param response Object to assist a servlet in sending a
     *                 response to the client.
     * @throws IOException If a I/O error occurs
     * @throws ServletException If an error with servlet occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Method that reacts on POST http request and send it on processing
     *
     * @param request Incoming request
     * @param response Object to assist a servlet in sending a
     *                 response to the client.
     * @throws IOException If a I/O error occurs
     * @throws ServletException If an error with servlet occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Method that process load user info.
     *
     * @param request Incoming request
     * @param response Object to assist a servlet in sending a
     *                 response to the client.
     * @throws IOException If a I/O error occurs
     * @throws ServletException If an error with servlet occurs
     */
    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String pathInfo = request.getPathInfo().replaceAll("/", "");
        try {
            int userId = Integer.parseInt(pathInfo);
            UserHandler userHandler = new UserHandler();
            User user = userHandler.getUser(userId);
            request.setAttribute("user", user);
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(
                    ConfigurationManager.getProperty("path.page.profile"));
            dispatcher.forward(request, response);
        } catch (NumberFormatException | RequestOutOfRangeException ex) {
            logger.info("404. Requested URI is not found.");
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(
                    ConfigurationManager.getProperty("path.page.404"));
            dispatcher.forward(request, response);
        } catch (Exception ex) {
            logger.error("Error while loading user's profile page. " + ex.toString());
        }
    }
}
