package com.epam.courses.forum.servlets;
/**
 * SwitchLocaleServlet is a class that switch languages
 *
 * @author Murdeus
 * @version 1.0
 * @see javax.servlet.http.HttpServlet
 * @since 1.0
 */
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/switchLocale")
public class SwitchLocaleServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(SwitchLocaleServlet.class);

    /**
     * Method that reacts on GET http request and send it on processing
     *
     * @param request Incoming request
     * @param response Object to assist a servlet in sending a
     *                 response to the client.
     * @throws IOException If a I/O error occurs
     * @throws ServletException If an error with servlet occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Method that reacts on POST http request and send it on processing
     *
     * @param request Incoming request
     * @param response Object to assist a servlet in sending a
     *                 response to the client.
     * @throws IOException If a I/O error occurs
     * @throws ServletException If an error with servlet occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Method that sets language to ru or en depends on input parameter.
     *
     * @param request Incoming request
     * @param response Object to assist a servlet in sending a
     *                 response to the client.
     * @throws IOException If a I/O error occurs
     * @throws ServletException If an error with servlet occurs
     */
    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        if ("ru".equals(request.getParameter("lang"))) {
            request.getSession().setAttribute("language", "ru");
        } else {
            if ("en".equals(request.getParameter("lang"))) {
                request.getSession().setAttribute("language", "en");
            } else {
                logger.warn("Cannot get requested locale.");
            }
        }
        response.sendRedirect(request.getHeader("Referer"));
    }
}
