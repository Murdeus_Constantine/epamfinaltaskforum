package com.epam.courses.forum.servlets;
/**
 * RegistrationServlet is a class that responsible for registration.
 *
 * @author Murdeus
 * @version 1.0
 * @see javax.servlet.http.HttpServlet
 * @since 1.0
 */
import com.epam.courses.forum.handlers.RegistrationHandler;
import org.apache.log4j.Logger;
import com.epam.courses.forum.resourcemanager.ConfigurationManager;
import com.epam.courses.forum.resourcemanager.MessageManager;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/register")
public class RegistrationServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(RegistrationServlet.class);

    /**
     * Method that reacts on GET http request and send it on processing
     *
     * @param request Incoming request
     * @param response Object to assist a servlet in sending a
     *                 response to the client.
     * @throws IOException If a I/O error occurs
     * @throws ServletException If an error with servlet occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("Registration is not reachable in GET request.");
        response.sendRedirect(ConfigurationManager.getProperty("path.page.index"));
    }

    /**
     * Method that reacts on POST http request and send it on processing
     *
     * @param request Incoming request
     * @param response Object to assist a servlet in sending a
     *                 response to the client.
     * @throws IOException If a I/O error occurs
     * @throws ServletException If an error with servlet occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        if (request.getParameter("command") != null) {
            RegistrationHandler registrationHandler = new RegistrationHandler();
            if (registrationHandler.validate(request)) {
                if (registrationHandler.register(request)) {
                    logger.info("Register new user.");
                    response.sendRedirect(ConfigurationManager.getProperty("path.page.index"));
                } else {
                    logger.warn("Database error while creating user.");
                }
            } else {
                logger.info("Incorrect input in registration fields or user has already been registered.");
                request.setAttribute("errorRegisterMessage", MessageManager.getProperty("message.register.error"));
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(
                        ConfigurationManager.getProperty("path.page.register"));
                dispatcher.forward(request, response);
            }
        } else {
            logger.info("Empty command in register request.");
            response.sendRedirect(ConfigurationManager.getProperty("path.page.index"));
        }
    }
}
