package com.epam.courses.forum.servlets;
/**
 * UserDeleteServlet is a class that response to delete users.
 *
 * @author Murdeus
 * @version 1.0
 * @see javax.servlet.http.HttpServlet
 * @since 1.0
 */
import com.epam.courses.forum.handlers.UserHandler;
import com.epam.courses.forum.resourcemanager.ConfigurationManager;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/deleteuser/*")
public class UserDeleteServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(UserDeleteServlet.class);

    /**
     * Method that reacts on GET http request and send it on processing
     *
     * @param request Incoming request
     * @param response Object to assist a servlet in sending a
     *                 response to the client.
     * @throws IOException If a I/O error occurs
     * @throws ServletException If an error with servlet occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("Delete user command is not reachable in GET request.");
        response.sendRedirect(ConfigurationManager.getProperty("path.page.index"));
    }

    /**
     * Method that reacts on POST http request and send it on processing
     *
     * @param request Incoming request
     * @param response Object to assist a servlet in sending a
     *                 response to the client.
     * @throws IOException If a I/O error occurs
     * @throws ServletException If an error with servlet occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Method that process delete user. This action must be invoke only
     * in admin role.
     *
     * @param request Incoming request
     * @param response Object to assist a servlet in sending a
     *                 response to the client.
     * @throws IOException If a I/O error occurs
     * @throws ServletException If an error with servlet occurs
     */
    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String pathInfo = request.getPathInfo().replaceAll("/", "");
        try {
            int userId = Integer.parseInt(pathInfo);
            UserHandler userHandler = new UserHandler();
            if ("admin".equals(request.getSession().getAttribute("userRole"))) {
                if ((Integer) request.getSession().getAttribute("userId") != userId) {
                    response.setContentType("text/plain");
                    response.setCharacterEncoding("UTF-8");
                    if (userHandler.deleteUser(userId)) {
                        response.getWriter().write("success");
                    } else {
                        logger.warn("Database error while deleting user.");
                        response.getWriter().write("fail");
                    }
                } else {
                    logger.warn("Cannot delete yourself.");
                    response.getWriter().write("fail");
                }
            } else {
                logger.info("Deny attempt to delete other user.");
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(
                        ConfigurationManager.getProperty("path.page.access.deny"));
                dispatcher.forward(request, response);
            }
        } catch (NumberFormatException ex) {
            logger.info("404. Requested URI is not found.");
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(
                    ConfigurationManager.getProperty("path.page.404"));
            dispatcher.forward(request, response);
        } catch (Exception ex) {
            logger.error("Error while deleting user. " + ex.toString());
        }
    }
}
