package com.epam.courses.forum.servlets;
/**
 * PostCreateServlet is a class that provide an opportunity
 * to create posts.
 *
 * @author Murdeus
 * @version 1.0
 * @see javax.servlet.http.HttpServlet
 * @since 1.0
 */
import com.epam.courses.forum.handlers.PostHandler;
import org.apache.log4j.Logger;
import com.epam.courses.forum.resourcemanager.ConfigurationManager;
import com.epam.courses.forum.resourcemanager.MessageManager;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/postsubmit")
public class PostCreateServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(PostCreateServlet.class);

    /**
     * Method that reacts on GET http request and send it on processing
     *
     * @param request Incoming request
     * @param response Object to assist a servlet in sending a
     *                 response to the client.
     * @throws IOException If a I/O error occurs
     * @throws ServletException If an error with servlet occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Method that reacts on POST http request and send it on processing
     *
     * @param request Incoming request
     * @param response Object to assist a servlet in sending a
     *                 response to the client.
     * @throws IOException If a I/O error occurs
     * @throws ServletException If an error with servlet occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Method that process a post creation and redirects user
     * depends on operation success.
     *
     * @param request Incoming request
     * @param response Object to assist a servlet in sending a
     *                 response to the client.
     * @throws IOException If a I/O error occurs
     * @throws ServletException If an error with servlet occurs
     */
    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        if (request.getSession().getAttribute("userId") != null) {
            if (request.getParameter("command") != null) {
                PostHandler postHandler = new PostHandler();
                if (postHandler.validate(request.getParameter("inputPostTopic"), request.getParameter("inputPostBody"))) {
                    if (postHandler.submitPost(request)) {
                        response.sendRedirect(ConfigurationManager.getProperty("path.page.index"));
                    } else {
                        logger.warn("Database error while creating post.");
                    }
                } else {
                    logger.info("Incorrect input values into post create form.");
                    request.setAttribute("errorPostCreationMessage", MessageManager.getProperty("message.post.create.error"));
                    RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(
                            ConfigurationManager.getProperty("path.page.post.create"));
                    dispatcher.forward(request, response);
                }
            } else {
                logger.info("Empty command in post create request.");
                response.sendRedirect(ConfigurationManager.getProperty("path.page.index"));
            }
        } else {
            logger.info("Deny attempt to create post while not log in.");
            response.sendRedirect(ConfigurationManager.getProperty("path.page.login"));
        }
    }
}
