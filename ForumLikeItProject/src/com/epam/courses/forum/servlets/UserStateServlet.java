package com.epam.courses.forum.servlets;
/**
 * UserStateServlet is a class that responsible for ban
 * and unban users by switching their state.
 *
 * @author Murdeus
 * @version 1.0
 * @see javax.servlet.http.HttpServlet
 * @since 1.0
 */
import com.epam.courses.forum.exceptions.RequestOutOfRangeException;
import com.epam.courses.forum.handlers.LikeHandler;
import com.epam.courses.forum.handlers.UserHandler;
import com.epam.courses.forum.resourcemanager.ConfigurationManager;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/changeUser")
public class UserStateServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(UserStateServlet.class);

    /**
     * Method that reacts on GET http request and send it on processing
     *
     * @param request Incoming request
     * @param response Object to assist a servlet in sending a
     *                 response to the client.
     * @throws IOException If a I/O error occurs
     * @throws ServletException If an error with servlet occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("Change user's state command is not reachable in GET request.");
        response.sendRedirect(ConfigurationManager.getProperty("path.page.index"));
    }

    /**
     * Method that reacts on POST http request and send it on processing
     *
     * @param request Incoming request
     * @param response Object to assist a servlet in sending a
     *                 response to the client.
     * @throws IOException If a I/O error occurs
     * @throws ServletException If an error with servlet occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Method that process changing user state action.
     *
     * @param request Incoming request
     * @param response Object to assist a servlet in sending a
     *                 response to the client.
     * @throws IOException If a I/O error occurs
     * @throws ServletException If an error with servlet occurs
     */
    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        try {
            int userId = Integer.parseInt(request.getParameter("idUser"));
            UserHandler userHandler = new UserHandler();
            if ("admin".equals(request.getSession().getAttribute("userRole"))) {
                if ((Integer) request.getSession().getAttribute("userId") != userId) {
                    userHandler.changeUserState(userId);
                    response.setContentType("text/plain");
                    response.setCharacterEncoding("UTF-8");
                    response.getWriter().write("" + userHandler.getUserState());
                } else {
                    logger.warn("Cannot change self state.");
                    response.getWriter().write("true");
                }
            }
        } catch (SQLException ex) {
            logger.error("Error with change user's state action. " + ex.toString());
        } catch (NumberFormatException | RequestOutOfRangeException ex) {
            logger.info("404. Requested URI is not found.");
        }
    }
}
