package com.epam.courses.forum.servlets;
/**
 * CommentEditPrepareServlet is a class that provide an opportunity
 * to prepare comment for editing. However it's not response for directly
 * edit them.
 *
 * @author Murdeus
 * @version 1.0
 * @see javax.servlet.http.HttpServlet
 * @since 1.0
 */
import com.epam.courses.forum.entity.Comment;
import com.epam.courses.forum.exceptions.RequestOutOfRangeException;
import com.epam.courses.forum.handlers.CommentHandler;
import org.apache.log4j.Logger;
import com.epam.courses.forum.resourcemanager.ConfigurationManager;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/preparecomment/*")
public class CommentEditPrepareServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(CommentEditPrepareServlet.class);

    /**
     * Method that reacts on GET http request and send it on processing
     *
     * @param request Incoming request
     * @param response Object to assist a servlet in sending a
     *                 response to the client.
     * @throws IOException If a I/O error occurs
     * @throws ServletException If an error with servlet occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Method that reacts on POST http request and send it on processing
     *
     * @param request Incoming request
     * @param response Object to assist a servlet in sending a
     *                 response to the client.
     * @throws IOException If a I/O error occurs
     * @throws ServletException If an error with servlet occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Method that process a comment edition operation, checks user's ability
     * to edit them and redirects user.
     *
     * @param request Incoming request
     * @param response Object to assist a servlet in sending a
     *                 response to the client.
     * @throws IOException If a I/O error occurs
     * @throws ServletException If an error with servlet occurs
     */
    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String pathInfo = request.getPathInfo().replaceAll("/", "");
        try {
            int commentId = Integer.parseInt(pathInfo);
            CommentHandler commentHandler = new CommentHandler();
            Comment comment = commentHandler.getComment(commentId);
            if (request.getSession().getAttribute("userId") != null) {
                if ((comment.getUserId() == (Integer) request.getSession().getAttribute("userId")) ||
                ("admin".equals(request.getSession().getAttribute("userRole")))){
                    request.setAttribute("changeCommentId", commentId);
                    RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(
                            "/post/" + comment.getMessageId());
                    dispatcher.forward(request, response);
                } else {
                    logger.info("Deny attempt to change other user's comment.");
                    RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(
                            ConfigurationManager.getProperty("path.page.access.deny"));
                    dispatcher.forward(request, response);
                }
            } else {
                logger.info("Deny attempt to change other user's comment while not log in.");
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(
                        ConfigurationManager.getProperty("path.page.access.deny"));
                dispatcher.forward(request, response);
            }
        } catch (NumberFormatException | RequestOutOfRangeException ex) {
            logger.info("404. Requested URI is not found.");
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(
                    ConfigurationManager.getProperty("path.page.404"));
            dispatcher.forward(request, response);
        } catch (Exception ex) {
            logger.error("Error while deleting comment." + ex.toString());
        }
    }
}
