package com.epam.courses.forum.dao;
/**
 * RatingDao is a DAO class for rating.
 * Contains CRUD methods, overridden from AbstractDao.
 * Some methods are not supported.
 *
 * @author Murdeus
 * @version 1.0
 * @see com.epam.courses.forum.dao.AbstractDao
 * @since 1.0
 */
import com.epam.courses.forum.entity.Rating;
import com.epam.courses.forum.exceptions.RequestOutOfRangeException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;

public class RatingDao extends AbstractDao<Rating> {

    private static final String ADD_STATEMENT = "INSERT INTO forum_likeit.rating (comment_id, user_id, rating, rating_time) VALUES (?,?,?,?)";
    private static final String GET_INFO_STATEMENT = "SELECT comment_id, user_id, rating, rating_time " +
            "FROM forum_likeit.rating WHERE comment_id=? AND user_id=? ORDER BY rating_time DESC";
    private static final String UPDATE_STATEMENT = "UPDATE forum_likeit.rating SET rating=? WHERE comment_id=? AND user_id=?";

    /**
     * Class constructor.
     *
     * @param connection Connection from pool.
     */
    public RatingDao(Connection connection) {
        super(connection);
    }

    /**
     * Method for creation rating.
     *
     * @param rating Rating.
     * @return result of creation.
     * @throws SQLException If a database access
     *                      error occurs
     */
    @Override
    public int create(Rating rating) throws SQLException {
        int result = 0;
        PreparedStatement statement = connection.prepareStatement(ADD_STATEMENT);
        statement.setInt(1, rating.getCommentId());
        statement.setInt(2, rating.getUserId());
        statement.setInt(3, rating.getRating());
        SimpleDateFormat sdf =
                new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String currentTime = sdf.format(rating.getRatingDate());
        statement.setString(4, currentTime);
        result = statement.executeUpdate();
        closeStatement(statement);
        return result;
    }

    /**
     * Method for getting all rating.
     * Unsupported
     *
     * @param id id of related entity.
     */
    @Override
    public List<Rating> getAll(int id) {
        throw new UnsupportedOperationException();
    }

    /**
     * Method for deleting all rating.
     * Unsupported
     *
     * @param id id of rating.
     */
    @Override
    public int deleteEntityById(int id) {
        throw new UnsupportedOperationException();
    }

    /**
     * Method for getting rating by it's id.
     *
     * @param id Id of rating.
     * @return rating itself.
     * @throws SQLException If a database access
     *                      error occurs
     * @throws RequestOutOfRangeException If rating doesn't
     *                                    exist in DB.
     */
    @Override
    public Rating getEntityById(int id) throws SQLException, RequestOutOfRangeException {
        throw new UnsupportedOperationException();
    }

    /**
     * Method for update rating.
     *
     * @param rating Updated rating
     * @return result of update.
     */
    @Override
    public int update(Rating rating) {
        int result = 0;
        try {
            PreparedStatement statement = connection.prepareStatement(UPDATE_STATEMENT);
            statement.setInt(1, rating.getRating());
            statement.setInt(2, rating.getCommentId());
            statement.setInt(3, rating.getUserId());
            result = statement.executeUpdate();
            closeStatement(statement);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    /**
     * Method for getting rating by comment and user id.
     *
     * @param commentId Id of related comment.
     * @param userId Id of related user.
     * @return rating itself.
     * @throws SQLException If a database access
     *                      error occurs
     */
    public Rating getRatingInfo(int commentId, int userId) throws SQLException {
        Rating rating = new Rating();
        try {
            PreparedStatement statement = connection.prepareStatement(GET_INFO_STATEMENT);
            statement.setInt(1, commentId);
            statement.setInt(2, userId);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                rating.setUserId(resultSet.getInt("user_id"));
                rating.setCommentId(resultSet.getInt("comment_id"));
                rating.setRating(resultSet.getInt("rating"));
                rating.setRatingDate(resultSet.getTimestamp("rating_time"));
                closeStatement(statement);
                return rating;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
