package com.epam.courses.forum.dao;
/**
 * AbstractDao is abstract class for all dao in project.
 * Contains common CRUD methods.
 *
 * @author Murdeus
 * @version 1.0
 * @since 1.0
 */
import com.epam.courses.forum.entity.Entity;
import com.epam.courses.forum.exceptions.RequestOutOfRangeException;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public abstract class AbstractDao<T extends Entity> {

    private static final Logger logger = Logger.getLogger(AbstractDao.class);
    protected Connection connection;

    /**
     * Class constructor.
     *
     * @param connection Connection from pool.
     */
    public AbstractDao(Connection connection) {
        this.connection = connection;
    }

    /**
     * Abstract method for getting all entities.
     *
     * @param id Id of related entity, which connected
     *           to whole list.
     * @return list of entities.
     */
    public abstract List<T> getAll(int id);

    /**
     * Abstract method for getting entity by it's id.
     *
     * @param id Id of entity.
     * @return entity itself.
     * @throws SQLException If a database access
     *                      error occurs
     * @throws RequestOutOfRangeException If entity doesn't
     *                                    exist in DB.
     */
    public abstract T getEntityById(int id) throws SQLException, RequestOutOfRangeException;

    /**
     * Abstract method for deleting entity by it's id.
     *
     * @param id Id of entity.
     * @return result of deleting.
     */
    public abstract int deleteEntityById(int id);

    /**
     * Abstract method for creation entity.
     *
     * @param entity Entity.
     * @return result of creation.
     */
    public abstract int create(T entity) throws SQLException;

    /**
     * Abstract method for update entity.
     *
     * @param entity Entity.
     * @return result of deleting.
     */
    public abstract int update(T entity);

    /**
     * Method for closing statement.
     *
     * @param statement Statement that need to be closed.
     */
    public void closeStatement(Statement statement) {
        try {
            if (statement != null) {
                statement.close();
            }
        } catch (SQLException ex) {
            logger.warn("Database error while closing statement.");
        }
    }
}
