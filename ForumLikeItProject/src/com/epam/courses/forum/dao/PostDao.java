package com.epam.courses.forum.dao;
/**
 * PostDao is a DAO class for posts.
 * Contains CRUD methods, overridden from AbstractDao.
 *
 * @author Murdeus
 * @version 1.0
 * @see com.epam.courses.forum.dao.AbstractDao
 * @since 1.0
 */
import com.epam.courses.forum.entity.Post;
import com.epam.courses.forum.exceptions.RequestOutOfRangeException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class PostDao extends AbstractDao<Post> {

    private static final String ADD_STATEMENT = "INSERT INTO forum_likeit.message (user_id, text, topic, post_time) VALUES (?,?,?,?)";
    private static final String GET_LIST_STATEMENT = "SELECT id, user_id, text, topic, post_time " +
            "FROM forum_likeit.message ORDER BY post_time DESC LIMIT 25";
    private static final String GET_RELATED_LIST_STATEMENT = "SELECT id, user_id, text, topic, post_time " +
            "FROM forum_likeit.message WHERE user_id=? ORDER BY post_time DESC LIMIT 20";
    private static final String DELETE_STATEMENT = "DELETE FROM forum_likeit.message WHERE id=?";
    private static final String GET_INFO_STATEMENT = "SELECT id, user_id, text, topic, post_time " +
            "FROM forum_likeit.message WHERE id=?";
    private static final String UPDATE_STATEMENT = "UPDATE forum_likeit.message SET text=?, topic=? WHERE id=?";

    /**
     * Class constructor.
     *
     * @param connection Connection from pool.
     */
    public PostDao(Connection connection) {
        super(connection);
    }

    /**
     * Method for creation post.
     *
     * @param post Post.
     * @return result of creation.
     * @throws SQLException If a database access
     *                      error occurs
     */
    @Override
    public int create(Post post) throws SQLException {
        int result = 0;
        PreparedStatement statement = connection.prepareStatement(ADD_STATEMENT);
        statement.setInt(1, post.getUserId());
        statement.setString(2, post.getMessageText());
        statement.setString(3, post.getMessageTopic());
        SimpleDateFormat sdf =
                new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String currentTime = sdf.format(post.getMessageDate());
        statement.setString(4, currentTime);
        result = statement.executeUpdate();
        closeStatement(statement);
        return result;
    }

    /**
     * Method for getting all comments related with concrete post.
     *
     * @param id 0, if doesn't related to
     *           any other entity.
     * @return list of posts.
     */
    @Override
    public List<Post> getAll(int id) {
        List<Post> posts = new ArrayList<Post>();
        PreparedStatement statement;
        try {
            if (id == 0) {
                statement = connection.prepareStatement(GET_LIST_STATEMENT);
            } else {
                statement = connection.prepareStatement(GET_RELATED_LIST_STATEMENT);
                statement.setInt(1, id);
            }
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Post post = new Post();
                post.setId(resultSet.getInt("id"));
                post.setUserId(resultSet.getInt("user_id"));
                post.setMessageText(resultSet.getString("text"));
                post.setMessageTopic(resultSet.getString("topic"));
                post.setMessageDate(resultSet.getTimestamp("post_time"));
                posts.add(post);
            }
            closeStatement(statement);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return posts;
    }

    /**
     * Method for deleting post by it's id.
     *
     * @param id Id of post.
     * @return result of deleting.
     */
    @Override
    public int deleteEntityById(int id) {
        int result = 0;
        try {
            PreparedStatement statement = connection.prepareStatement(DELETE_STATEMENT);
            statement.setInt(1, id);
            result = statement.executeUpdate();
            closeStatement(statement);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    /**
     * Method for getting post by it's id.
     *
     * @param id Id of post.
     * @return post itself.
     * @throws SQLException               If a database access
     *                                    error occurs
     * @throws RequestOutOfRangeException If post doesn't
     *                                    exist in DB.
     */
    @Override
    public Post getEntityById(int id) throws SQLException, RequestOutOfRangeException {
        Post post = new Post();
        try {
            PreparedStatement statements = connection.prepareStatement(GET_INFO_STATEMENT);
            statements.setInt(1, id);
            ResultSet resultSet = statements.executeQuery();
            if (resultSet.next()) {
                post.setId(resultSet.getInt("id"));
                post.setUserId(resultSet.getInt("user_id"));
                post.setMessageText(resultSet.getString("text"));
                post.setMessageTopic(resultSet.getString("topic"));
                post.setMessageDate(resultSet.getTimestamp("post_time"));
                closeStatement(statements);
                return post;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        throw new RequestOutOfRangeException();
    }

    /**
     * Method for update post.
     *
     * @param post Updated post
     * @return result of update.
     */
    @Override
    public int update(Post post) {
        int result = 0;
        try {
            PreparedStatement statement = connection.prepareStatement(UPDATE_STATEMENT);
            statement.setString(1, post.getMessageText());
            statement.setString(2, post.getMessageTopic());
            statement.setInt(3, post.getId());
            result = statement.executeUpdate();
            closeStatement(statement);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }
}
