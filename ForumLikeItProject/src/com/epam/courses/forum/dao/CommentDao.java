package com.epam.courses.forum.dao;
/**
 * CommentDao is a DAO class for comments.
 * Contains CRUD methods, overridden from AbstractDao.
 *
 * @author Murdeus
 * @version 1.0
 * @see com.epam.courses.forum.dao.AbstractDao
 * @since 1.0
 */
import com.epam.courses.forum.entity.Comment;
import com.epam.courses.forum.exceptions.RequestOutOfRangeException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CommentDao extends AbstractDao<Comment> {

    private static final String ADD_STATEMENT = "INSERT INTO forum_likeit.comment (user_id, message_id, text, comment_time) VALUES (?,?,?,?)";
    private static final String GET_LIST_STATEMENT = "SELECT id, user_id, message_id, text, rating, comment_time " +
            "FROM forum_likeit.comment WHERE message_id=? ORDER BY comment_time ASC";
    private static final String DELETE_STATEMENT = "DELETE FROM forum_likeit.comment WHERE id=?";
    private static final String GET_INFO_STATEMENT = "SELECT id, user_id, message_id, text, rating, comment_time " +
            "FROM forum_likeit.comment WHERE id=?";
    private static final String UPDATE_STATEMENT = "UPDATE forum_likeit.comment SET text=? WHERE id=?";

    /**
     * Class constructor.
     *
     * @param connection Connection from pool.
     */
    public CommentDao(Connection connection) {
        super(connection);
    }

    /**
     * Method for creation comment.
     *
     * @param comment Comment.
     * @return result of creation.
     * @throws SQLException If a database access
     *                      error occurs
     */
    @Override
    public int create(Comment comment) throws SQLException {
        int result = 0;
        PreparedStatement statement = connection.prepareStatement(ADD_STATEMENT);
        statement.setInt(1, comment.getUserId());
        statement.setInt(2, comment.getMessageId());
        statement.setString(3, comment.getCommentText());
        java.text.SimpleDateFormat sdf =
                new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String currentTime = sdf.format(comment.getCommentDate());
        statement.setString(4, currentTime);
        result = statement.executeUpdate();
        closeStatement(statement);
        return result;
    }

    /**
     * Method for getting all comments related with concrete post.
     *
     * @param id Id of related post.
     * @return list of comments.
     */
    @Override
    public List<Comment> getAll(int id) {
        List<Comment> comments = new ArrayList<Comment>();
        try {
            PreparedStatement statement = connection.prepareStatement(GET_LIST_STATEMENT);
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Comment com = new Comment();
                com.setId(resultSet.getInt("id"));
                com.setUserId(resultSet.getInt("user_id"));
                com.setMessageId(id);
                com.setCommentText(resultSet.getString("text"));
                com.setRating(resultSet.getInt("rating"));
                com.setCommentDate(resultSet.getTimestamp("comment_time"));
                comments.add(com);
            }
            closeStatement(statement);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return comments;
    }

    /**
     * Method for deleting comment by it's id.
     *
     * @param id Id of comment.
     * @return result of deleting.
     */
    @Override
    public int deleteEntityById(int id) {
        int result = 0;
        try {
            PreparedStatement statement = connection.prepareStatement(DELETE_STATEMENT);
            statement.setInt(1, id);
            result = statement.executeUpdate();
            closeStatement(statement);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    /**
     * Method for getting comment by it's id.
     *
     * @param id Id of comment.
     * @return comment itself.
     * @throws SQLException If a database access
     *                      error occurs
     * @throws RequestOutOfRangeException If comment doesn't
     *                                    exist in DB.
     */
    @Override
    public Comment getEntityById(int id) throws SQLException, RequestOutOfRangeException {
        Comment comment = new Comment();
        PreparedStatement statement = connection.prepareStatement(GET_INFO_STATEMENT);
        statement.setInt(1, id);
        ResultSet resultSet = statement.executeQuery();
        if (resultSet.next()) {
            comment.setId(resultSet.getInt("id"));
            comment.setUserId(resultSet.getInt("user_id"));
            comment.setMessageId(resultSet.getInt("message_id"));
            comment.setCommentText(resultSet.getString("text"));
            comment.setRating(resultSet.getInt("rating"));
            comment.setCommentDate(resultSet.getTimestamp("comment_time"));
            closeStatement(statement);
            return comment;
        }
        throw new RequestOutOfRangeException();
    }

    /**
     * Method for update comment.
     *
     * @param comment Updated comment
     * @return result of update.
     */
    @Override
    public int update(Comment comment) {
        int result = 0;
        try {
            PreparedStatement statement = connection.prepareStatement(UPDATE_STATEMENT);
            statement.setString(1, comment.getCommentText());
            statement.setInt(2, comment.getId());
            result = statement.executeUpdate();
            closeStatement(statement);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }
}
