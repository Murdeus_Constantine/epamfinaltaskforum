package com.epam.courses.forum.dao;
/**
 * UserDao is a DAO class for user.
 * Contains CRUD methods, overridden from AbstractDao.
 *
 * @author Murdeus
 * @version 1.0
 * @see com.epam.courses.forum.dao.AbstractDao
 * @since 1.0
 */
import com.epam.courses.forum.converter.PasswordConvert;
import com.epam.courses.forum.entity.User;
import com.epam.courses.forum.exceptions.RequestOutOfRangeException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDao extends AbstractDao<User> {

    private static final String ADD_STATEMENT = "INSERT INTO forum_likeit.user (login, password, full_name, email) VALUES (?,?,?,?)";
    private static final String CHECK_ENABLE_STATEMENT = "SELECT COUNT(*) AS total FROM forum_likeit.user WHERE login=? OR email=?";
    private static final String CHECK_EXIST_STATEMENT = "SELECT COUNT(*) AS total FROM forum_likeit.user WHERE login=? AND password=?";
    private static final String GET_INFO_STATEMENT = "SELECT id, login, role, full_name, email, rating , active" +
            " FROM forum_likeit.user WHERE id=?";
    private static final String GET_ID_STATEMENT = "SELECT id FROM forum_likeit.user WHERE login=? AND password=?";
    private static final String GET_LIST = "SELECT id, login, role, rating, active" +
            " FROM forum_likeit.user";
    private static final String DELETE_STATEMENT = "DELETE FROM forum_likeit.user WHERE id=?";
    private static final String UPDATE_STATEMENT = "UPDATE forum_likeit.user SET active=? WHERE id=?";

    /**
     * Class constructor.
     *
     * @param connection Connection from pool.
     */
    public UserDao(Connection connection) {
        super(connection);
    }

    /**
     * Method for creation user.
     *
     * @param user User.
     * @return result of creation.
     * @throws SQLException If a database access
     *                      error occurs
     */
    @Override
    public int create(User user) throws SQLException {
        int result = 0;
        PreparedStatement statement = connection.prepareStatement(ADD_STATEMENT);
        statement.setString(1, user.getLogin());
        PasswordConvert passConvert = new PasswordConvert();
        statement.setString(2, passConvert.convert(user.getPassword()));
        statement.setString(3, user.getFullname());
        statement.setString(4, user.getEmail());
        result = statement.executeUpdate();
        closeStatement(statement);
        return result;
    }

    /**
     * Method for getting all users related with concrete entity.
     *
     * @param id 0, because doesn't related to
     *           any other entity.
     * @return list of users.
     */
    @Override
    public List<User> getAll(int id) {
        List<User> users = new ArrayList<User>();
        try {
            PreparedStatement statement = connection.prepareStatement(GET_LIST);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                User user = new User();
                user.setId(resultSet.getInt("id"));
                user.setLogin(resultSet.getString("login"));
                user.setRating(resultSet.getInt("rating"));
                user.setRole(resultSet.getString("role"));
                user.setActive(resultSet.getBoolean("active"));
                users.add(user);
            }
            closeStatement(statement);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return users;
    }

    /**
     * Method for deleting all users.
     * Unsupported
     *
     * @param id id of user.
     */
    @Override
    public int deleteEntityById(int id) {
        int result = 0;
        try {
            PreparedStatement statement = connection.prepareStatement(DELETE_STATEMENT);
            statement.setInt(1, id);
            result = statement.executeUpdate();
            closeStatement(statement);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    /**
     * Method for getting user by it's id.
     *
     * @param id Id of user.
     * @return user itself.
     * @throws SQLException If a database access
     *                      error occurs
     * @throws RequestOutOfRangeException If rating doesn't
     *                                    exist in DB.
     */
    @Override
    public User getEntityById(int id) throws SQLException, RequestOutOfRangeException {
        User user = new User();
        try {
            PreparedStatement statement = connection.prepareStatement(GET_INFO_STATEMENT);
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                user.setId(id);
                user.setLogin(resultSet.getString("login"));
                user.setFullname(resultSet.getString("full_name"));
                user.setEmail(resultSet.getString("email"));
                user.setRating(resultSet.getInt("rating"));
                user.setRole(resultSet.getString("role"));
                user.setActive(resultSet.getBoolean("active"));
                closeStatement(statement);
                return user;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        throw new RequestOutOfRangeException();
    }

    /**
     * Method for update user.
     *
     * @param user Updated user
     * @return result of update.
     */
    @Override
    public int update(User user) {
        int result = 0;
        try {
            PreparedStatement statement = connection.prepareStatement(UPDATE_STATEMENT);
            statement.setBoolean(1, user.getActive());
            statement.setInt(2, user.getId());
            result = statement.executeUpdate();
            closeStatement(statement);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    /**
     * Method for checking if login or email are enable.
     *
     * @param user User with login and email fields
     * @return result enable or not.
     */
    public int checkIfEnable(User user) {
        int result = 1;
        try {
            PreparedStatement statement = connection.prepareStatement(CHECK_ENABLE_STATEMENT);
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getEmail());
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = resultSet.getInt("total");
            }
            closeStatement(statement);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    /**
     * Method for checking if user exist yet.
     *
     * @param user User with login and password fields
     * @return result exists or not.
     */
    public int checkIfExist(User user) {
        int result = 0;
        try {
            PreparedStatement statement = connection.prepareStatement(CHECK_EXIST_STATEMENT);
            statement.setString(1, user.getLogin());
            PasswordConvert passConvert = new PasswordConvert();
            statement.setString(2, passConvert.convert(user.getPassword()));
            ResultSet resultSets = statement.executeQuery();
            if (resultSets.next()) {
                result = resultSets.getInt("total");
            }
            closeStatement(statement);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    /**
     * Method for getting user id by login and password.
     *
     * @param user User with login and password fields
     * @return user's id.
     */
    public int getUserSessionId(User user) {
        int result = 0;
        try {
            PreparedStatement statement = connection.prepareStatement(GET_ID_STATEMENT);
            statement.setString(1, user.getLogin());
            PasswordConvert passConvert = new PasswordConvert();
            statement.setString(2, passConvert.convert(user.getPassword()));
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = resultSet.getInt("id");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }
}

