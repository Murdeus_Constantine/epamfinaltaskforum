package com.epam.courses.forum.entity;
/**
 * Post is a class that represents post in system.
 * Contains necessary fields and getters with setters.
 *
 * @author Murdeus
 * @version 1.0
 * @see com.epam.courses.forum.entity.Entity
 * @since 1.0
 */
import java.text.SimpleDateFormat;
import java.util.Date;

public class Post extends Entity{

    private int id;
    private int userId;
    private String messageText;
    private String messageTopic;
    private Date messageDate;
    private String userName;
    private int commentNumber;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public String getMessageTopic() {
        return messageTopic;
    }

    public void setMessageTopic(String messageTopic) {
        this.messageTopic = messageTopic;
    }

    public Date getMessageDate() {
        return messageDate;
    }

    /**
     * Method that returns message date in
     * dd/MM/yyyy hh:mm:ss a format.
     *
     * @return formatted date.
     */
    public String getMessageDateTime(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
        return dateFormat.format(messageDate);
    }

    public void setMessageDate(Date messageDate) {
        this.messageDate = messageDate;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getCommentNumber() {
        return commentNumber;
    }

    public void setCommentNumber(int commentNumber) {
        this.commentNumber = commentNumber;
    }
}
