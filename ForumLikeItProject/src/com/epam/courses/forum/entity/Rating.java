package com.epam.courses.forum.entity;
/**
 * Rating is a class that represents rating in system.
 * Contains necessary fields and getters with setters.
 *
 * @author Murdeus
 * @version 1.0
 * @see com.epam.courses.forum.entity.Entity
 * @since 1.0
 */
import java.util.Date;

public class Rating extends Entity{

    private int commentId;
    private int userId;
    private int rating;
    private Date ratingDate;

    public int getCommentId() {
        return commentId;
    }

    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public Date getRatingDate() {
        return ratingDate;
    }

    public void setRatingDate(Date ratingDate) {
        this.ratingDate = ratingDate;
    }
}
