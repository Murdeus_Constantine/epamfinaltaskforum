package com.epam.courses.forum.entity;
/**
 * User is a class that represents user in system.
 * Contains necessary fields and getters with setters.
 *
 * @author Murdeus
 * @version 1.0
 * @see com.epam.courses.forum.entity.Entity
 * @since 1.0
 */
public class User extends Entity {

    private int id;
    private String role;
    private String login;
    private String password;
    private String fullname;
    private String email;
    private int rating;
    private Boolean active;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
