package com.epam.courses.forum.entity;
/**
 * Comment is a class that represents comment in system.
 * Contains necessary fields and getters with setters.
 *
 * @author Murdeus
 * @version 1.0
 * @see com.epam.courses.forum.entity.Entity
 * @since 1.0
 */
import java.text.SimpleDateFormat;
import java.util.Date;

public class Comment extends Entity{

    private int id;
    private int userId;
    private int messageId;
    private String commentText;
    private int rating;
    private Date commentDate;
    private String userName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getMessageId() {
        return messageId;
    }

    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public Date getCommentDate() {
        return commentDate;
    }

    public void setCommentDate(Date commentDate) {
        this.commentDate = commentDate;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * Method that returns message date in
     * dd/MM/yyyy hh:mm:ss a format.
     *
     * @return formatted date.
     */
    public String getMessageDateTime(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
        return dateFormat.format(commentDate);
    }
}
