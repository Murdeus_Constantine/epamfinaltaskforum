import com.epam.courses.forum.handlers.PostHandler;
import junit.framework.TestCase;

/**
 * Created by smile on 08-Oct-16.
 */
public class PostHandlerTest extends TestCase {

    private String testTopic = " ";
    private String testMessage = "asd";

    public void testValidation(){
        PostHandler postHandler = new PostHandler();

        assertFalse(postHandler.validate(testTopic, testMessage));
    }
}
