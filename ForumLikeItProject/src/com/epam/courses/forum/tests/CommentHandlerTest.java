import com.epam.courses.forum.handlers.CommentHandler;
import junit.framework.TestCase;

/**
 * Created by smile on 08-Oct-16.
 */
public class CommentHandlerTest extends TestCase {

    private String testString = "Comment is here.";

    public void testValidation(){
        CommentHandler commentHandler = new CommentHandler();

        assertTrue(commentHandler.validate(testString));
    }
}
