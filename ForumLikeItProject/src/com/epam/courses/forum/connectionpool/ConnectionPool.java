package com.epam.courses.forum.connectionpool;
/**
 * ConnectionPool is a class that stands for creation and manipulation
 * connections.
 * Base on Java Naming and Directory Interface (JNDI) with MySQL.
 *
 * @author Murdeus
 * @version 1.0
 * @since 1.0
 */
import org.apache.log4j.Logger;

import javax.naming.InitialContext;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class ConnectionPool {

    private static final Logger logger = Logger.getLogger(ConnectionPool.class);
    private static final String SOURCE_NAME = "java:comp/env/jdbc/forum";
    private static DataSource dataSource;

    /*
        * Create a JNDI Initial context to be able to
        * lookup  the DataSource
        * Lookup the DataSource, which will be backed by a pool
        * that the application server provides.
    */
   static {
       try {
           InitialContext ctx = new InitialContext();
           dataSource = (DataSource) ctx.lookup(SOURCE_NAME);
       } catch (Exception ex) {
           logger.warn("Exception in connection pool initialization. " + ex.toString());
       }
   }

   /**
    * Class constructor.
    */
    private ConnectionPool() {}

    /**
     * Returns connection from DataSource.
     *
     * @return connection
     * @throws SQLException If a database access
     *                      error occurs
     */
    public static Connection getConnection() throws SQLException {
        Connection connection = dataSource.getConnection();
        return connection;
    }

    /**
     * Returns connection back to pool.
     *
     * @throws SQLException If a database access
     *                      error occurs
     */
    public static void closeConnection(Connection connection) throws SQLException {
        connection.close();
    }
}
