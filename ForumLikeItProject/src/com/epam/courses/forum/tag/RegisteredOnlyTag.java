package com.epam.courses.forum.tag;
/**
 * RegisteredOnlyTag is a class for tag which body will form
 * only when user is logged.
 *
 * @author Murdeus
 * @version 1.0
 * @see javax.servlet.http.HttpServlet
 * @since 1.0
 */
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

public class RegisteredOnlyTag extends TagSupport {

    /**
     * Process the end tag for this instance. This method is invoked by
     * the JSP page implementation object on all Tag handlers.
     *
     * @throws JspException If it will be invocation
     *                      of the errorpage machinery.
     */
    @Override
    public int doStartTag() throws JspException{
        if(pageContext.getSession().getAttribute("userId") != null){
            return EVAL_BODY_INCLUDE;
        } else {
            return SKIP_BODY;
        }
    }
}
