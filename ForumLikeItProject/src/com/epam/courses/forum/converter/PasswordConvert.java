package com.epam.courses.forum.converter;
/**
 * PasswordConvert is a class for encrypting user's password.
 *
 * @author Murdeus
 * @version 1.0
 * @since 1.0
 */
import org.apache.log4j.Logger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class PasswordConvert {

    private static final Logger logger = Logger.getLogger(PasswordConvert.class);

    /**
     * Method for encrypting password.
     *
     * @param password Password.
     * @return encrypted password.
     * @throws NoSuchAlgorithmException If a converter
     *                                  error occurs
     */
    public String convert(String password) {
        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(password.getBytes());
            byte[] bytes = md.digest();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString();
        } catch (NoSuchAlgorithmException ex) {
            logger.error("Error while encrypting password. " + ex.toString());
        }
        return generatedPassword;
    }
}
