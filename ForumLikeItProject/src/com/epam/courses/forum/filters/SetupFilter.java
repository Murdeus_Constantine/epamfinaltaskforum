package com.epam.courses.forum.filters;
/**
 * SetupFilter is a class that filters any request and sets
 * content type and encoding for request and response.
 *
 * @author Murdeus
 * @version 1.0
 * @see javax.servlet.Filter
 * @since 1.0
 */
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

@WebFilter(urlPatterns = "*")
public class SetupFilter implements Filter {

    public static final String CONTENT_TYPE = "text/html";
    public static final String CONTENT_ENCODING = "UTF-8";

    /**
     * The doFilter method of the Filter is called by the container
     * each time a request/response pair is passed through the chain
     * due to a client request for a resource at the end of the chain.
     * The FilterChain passed in to this method allows the Filter to
     * pass on the request and response to the next entity in the chain.
     *
     * @param servletRequest Incoming request
     * @param servletResponse Object to assist a servlet in sending a
     *                        response to the client.
     * @param filterChain Chain of a filtered request for a resource
     * @throws IOException If a I/O error occurs
     * @throws ServletException If an error with servlet occurs
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        servletResponse.setContentType(CONTENT_TYPE);
        servletRequest.setCharacterEncoding(CONTENT_ENCODING);
        servletResponse.setContentType(CONTENT_TYPE);
        servletResponse.setCharacterEncoding(CONTENT_ENCODING);
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
