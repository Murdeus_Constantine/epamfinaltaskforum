package com.epam.courses.forum.resourcemanager;
/**
 * ConfigurationManager is a class that sets up a resource bundle
 * that config project paths.
 *
 * @author Murdeus
 * @version 1.0
 * @since 1.0
 */
import java.util.ResourceBundle;

public class ConfigurationManager {

    private static final ResourceBundle resourceBundle = ResourceBundle.getBundle("com/epam/courses/forum/resources/config");

    /**
     * Class constructor.
     */
    private ConfigurationManager() {}

    public static String getProperty(String key){
        return resourceBundle.getString(key);
    }
}
