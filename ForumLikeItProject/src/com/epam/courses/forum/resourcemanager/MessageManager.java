package com.epam.courses.forum.resourcemanager;
/**
 * MessageManager is a class that sets up a resource bundle
 * that config project messages.
 *
 * @author Murdeus
 * @version 1.0
 * @since 1.0
 */
import java.util.ResourceBundle;

public class MessageManager {

    private static final ResourceBundle resourceBundle = ResourceBundle.getBundle("com/epam/courses/forum/resources/text");

    /**
     * Class constructor.
     */
    private MessageManager() {}

    public static String getProperty(String key){
        return resourceBundle.getString(key);
    }

}
