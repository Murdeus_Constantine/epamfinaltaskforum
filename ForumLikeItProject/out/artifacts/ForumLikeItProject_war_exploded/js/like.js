function addLike(idLike) {
    $("#likeId"+idLike).parent().prop('disabled', true);
    $.post("/addLike", {
        "idComment" : idLike
    }, function successLike(data) {
        if(data!="fail") {
            $("#likeId" + idLike).text(" " + data);
        }
        $("#likeId"+idLike).parent().prop('disabled', false);
    });
}
