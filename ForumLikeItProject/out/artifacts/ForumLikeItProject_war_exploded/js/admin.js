function check(){
    $("input:checkbox:checked").each(function(){
        var id = $(this).parent("td").parent("tr").children(".idUser").text();
        deleteUser(id);
    });
}

function deleteUser(userId){
    $.post("/deleteuser/"+ userId, {
        },function successDelete(data) {
            if(data == "success") {
                $("#trId" + userId).remove();
            }else{
                $("#trId" + userId).children(".idCheck").children("input").attr('checked', false);
            }
        });
}

function editSelectedUsers(){
    $("input:checkbox:checked").each(function(){
        var id = $(this).parent("td").parent("tr").children(".idUser").text();
        editUser(id);
    });
}

function editUser(id){
    $.post("/changeUser", {
        "idUser" : id
    },function successDelete(data) {
        $("#actId"+id).text(" "+data);
        $("#trId" + id).children(".idCheck").children("input").attr('checked', false);
    });
}