<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="cy" uri="customtags" %>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="com.epam.courses.forum.resources.text" />

<!DOCTYPE html>
<html lang="${language}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Post page of LikeIt Forum">
  <meta name="author" content="Murdeus">

  <title>LikeIt - Post Page</title>

  <!-- Bootstrap Core CSS -->
  <link href="/css/bootstrap.css" rel="stylesheet">

  <!-- Custom CSS -->
  <link href="/css/custom.css" rel="stylesheet">
  <link href="/css/common.css" rel="stylesheet">

  <!-- jQuery -->
  <script src="/js/jquery.js"></script>

  <!-- Bootstrap Core JavaScript -->
  <script src="/js/bootstrap.min.js"></script>
  <script src="/js/like.js"></script>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body>
<nav class="navbar navbar-inverse navbar-fixed-top" >
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/index"><fmt:message key="label.home"/></a>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav ">
        <cy:registered-only>
          <li>
            <a href="/myposts"><fmt:message key="label.my.post"/></a>
          </li>
        </cy:registered-only>
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown"><fmt:message key="label.language"/>
            <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="/switchLocale?lang=en">English</a></li>
            <li><a href="/switchLocale?lang=ru">Русский</a></li>
          </ul>
        </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <c:if test="${'admin' == sessionScope.userRole}">
          <li>
            <a href="/admin">
              <span class="glyphicon glyphicon-tower"></span><fmt:message key="label.admin.page"/></a>
          </li>
        </c:if>
        <c:choose>
          <c:when test="${sessionScope.userId == null}">
            <li>
              <a href="/signup"><fmt:message key="label.sign.up"/></a>
            </li>
            <li>
              <a href="/signin">
                <span class="glyphicon glyphicon-log-in"></span> <fmt:message key="label.sign.in"/></a>
            </li>
          </c:when>
          <c:otherwise>
            <li>
              <a href="/user/${sessionScope.userId}"><span class="glyphicon glyphicon-user"></span> <fmt:message key="label.profile.page"/></a>
            </li>
            <li>
              <a href="/logout">
                <span class="glyphicon glyphicon-log-out" ></span><fmt:message key="label.log.out"/></a>
            </li>
          </c:otherwise>
        </c:choose>
      </ul>
    </div>
  </div>
</nav>

<div class="container">
  <div class="row">
    <section class="col-lg-9">
      <h1><c:out value="${post.messageTopic}" /></h1>
      <p><span class="glyphicon glyphicon-time"></span> <fmt:message key="label.posted.on"/> <c:out value="${post.getMessageDateTime()}" /> <fmt:message key="label.posted.by"/> <a href="/user/${post.getUserId()}"><c:out value="${post.getUserName()}" /></a></p>
      <hr>
      <div class="well">
        <p><c:out value="${post.messageText}" /></p>
      </div>

      <c:if test="${(sessionScope.userLogin == post.getUserName())||('admin' == sessionScope.userRole)}">
        <div class="comment-panel">
          <a href="/preparepost/${post.getId()}">
            <button type="button" class="btn btn-default btn-sm btn-edit">
              <span class="glyphicon glyphicon-pencil"></span><fmt:message key="label.button.question.edit"/>
            </button>
          </a>
          <a href="/deletepost/${post.getId()}">
            <button type="button" class="btn btn-default btn-sm btn-del">
              <span class="glyphicon glyphicon-remove"></span><fmt:message key="label.admin.delete"/>
            </button>
          </a>
        </div>
      </c:if>

      <hr>
      <!-- Posted Comments -->
      <!-- Comment -->
<a name="comments"></a>
<c:forEach items="${list}" var="comment">
  <c:choose>
  <c:when test="${comment.getId() == changeCommentId}">
    <div class="well comment-form">
      <form action="commentedit" method="post">
        <input type="hidden" name="command" value="commentedit">
        <input type="hidden" name="postId" value=${post.getId()}>
        <input type="hidden" name="commentEditId" value=${changeCommentId}>
        <div class="form-group">
          <textarea maxlength="128" required id="inputCommentEdit" name="inputCommentEdit"  class="form-control" rows="4"><c:out value="${comment.getCommentText()}" /></textarea>
            ${errorCommentEditMessage}
        </div>
        <button type="submit" class="btn btn-edit btn-primary"><fmt:message key="label.button.question.edit"/></button>
      </form>
    </div>
  </c:when>
  <c:otherwise>
      <div class="media">
        <h4 class="media-heading"><a href="/user/${comment.getUserId()}"><c:out value="${comment.getUserName()}" /></a>
          <small><c:out value="${comment.getMessageDateTime()}" /> </small>
        </h4>
        <div class="media-body">
          <c:out value="${comment.getCommentText()}" />
        </div>
        <div class="comment-panel">
          <button  type="button" class="btn btn-default btn-sm" onclick="addLike(${comment.getId()})">
             <span id="likeId${comment.getId()}" class="glyphicon glyphicon-thumbs-up"> <c:out value="${comment.getRating()}" /></span>
          </button>

          <c:if test="${(sessionScope.userLogin == comment.getUserName())||('admin' == sessionScope.userRole)}">
            <a href="/preparecomment/${comment.getId()}">
              <button type="button" class="btn btn-default btn-sm btn-edit">
                <span class="glyphicon glyphicon-pencil"></span><fmt:message key="label.button.question.edit"/>
              </button>
            </a>
            <a href="/deletecomment/${comment.getId()}">
              <button type="button" class="btn btn-default btn-sm btn-del">
                <span class="glyphicon glyphicon-remove"></span><fmt:message key="label.admin.delete"/>
              </button>
            </a>
          </c:if>
        </div>
      </div>
      <hr>
  </c:otherwise>
  </c:choose>
</c:forEach>

      <!-- Comments Form -->
<c:if test="${sessionScope.userId != null}">
  <c:if test="${changeCommentId==null}">
      <div class="well comment-form">
        <h4><fmt:message key="label.post.comment"/></h4>
        <form action="commentsubmit">
          <input type="hidden" name="command" value="commentsubmit">
          <input type="hidden" name="postId" value=${post.getId()}>
          <div class="form-group">
            <textarea maxlength="128" required id="inputComment" name="inputComment" placeholder=<fmt:message key="label.your.comment"/> class="form-control" rows="4"></textarea>
            ${errorCommentCreationMessage}
          </div>
          <button type="submit" class="btn btn-edit btn-primary"><fmt:message key="label.button.comment.post"/></button>
        </form>
      </div>
  </c:if>
</c:if>
    </section>

    <div class="col-md-3">

      <!-- Some news -->
      <aside class="well">
        <h4><fmt:message key="label.recent.news.header"/></h4>
        <p><fmt:message key="label.recent.news.body"/></p>
      </aside>
      <aside>
        <a href="/createpost"><button class="btn btn-large btn-block question-btn" type="submit"><fmt:message key="label.button.ask.qustions"/></button></a>
      </aside>

    </div>
  </div>
</div>

<footer>
  <p>Copyright &copy; Murdeus Website 2016</p>
</footer>
</body>
</html>

