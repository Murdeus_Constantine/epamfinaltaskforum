<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="cy" uri="customtags" %>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="com.epam.courses.forum.resources.text" />

<!DOCTYPE html>
<html lang="${language}">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Home page of LikeIt Forum">
  <meta name="author" content="Murdeus">

  <title>LikeIt - <fmt:message key="label.home"/></title>

  <!-- Bootstrap Core CSS -->
  <link href="/css/bootstrap.css" rel="stylesheet">

  <!-- Custom CSS -->
  <link href="/css/custom.css" rel="stylesheet">

  <!-- jQuery -->
  <script src="/js/jquery.js"></script>

  <!-- Bootstrap Core JavaScript -->
  <script src="/js/bootstrap.min.js"></script>

  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body>
<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" >
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/index"><fmt:message key="label.home"/></a>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav ">
        <cy:registered-only>
          <li>
            <a href="/myposts"><fmt:message key="label.my.post"/></a>
          </li>
        </cy:registered-only>
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown"><fmt:message key="label.language"/>
            <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="/switchLocale?lang=en">English</a></li>
            <li><a href="/switchLocale?lang=ru">Русский</a></li>
          </ul>
        </li>

      </ul>
      <ul class="nav navbar-nav navbar-right">
        <c:if test="${'admin' == sessionScope.userRole}">
          <li>
            <a href="/admin">
              <span class="glyphicon glyphicon-tower"></span><fmt:message key="label.admin.page"/></a>
          </li>
        </c:if>
        <c:choose>
        <c:when test="${sessionScope.userId == null}">
          <li>
            <a href="/signup"><fmt:message key="label.sign.up"/></a>
          </li>
          <li>
            <a href="/signin">
              <span class="glyphicon glyphicon-log-in"></span> <fmt:message key="label.sign.in"/></a>
          </li>
        </c:when>
        <c:otherwise>
          <li>
            <a href="/user/${sessionScope.userId}"><span class="glyphicon glyphicon-user"></span> <fmt:message key="label.profile.page"/></a>
          </li>
          <li>
            <a href="/logout">
              <span class="glyphicon glyphicon-log-out" ></span><fmt:message key="label.log.out"/></a>
          </li>
        </c:otherwise>
      </c:choose>
      </ul>
    </div>
  </div>
</nav>

<!-- Page Content -->
<div class="container">
  <div class="row">
    <!-- Blog Entries Column -->
    <section class="col-md-9" id="posts">
      <h1 class="page-header">
        <fmt:message key="label.last.questions"/>
      </h1>
      <!-- Post -->
      <c:forEach items="${list}" var="post">
        <h3>
          <a href="/post/${post.getId()}"><c:out value="${post.getMessageTopic()}" /></a>
        </h3>
        <p><span class="glyphicon glyphicon-time"></span> <fmt:message key="label.posted.on"/> <c:out value="${post.getMessageDateTime()}" /> <fmt:message key="label.posted.by"/> <a href="/user/${post.getUserId()}"><c:out value="${post.getUserName()}" /></a></p>
        <div class="col-sm-12 message">
          <div class="col-sm-12 message-text">
            <div class="col-sm-10">
              <div class="well">
                <p><c:out value="${post.getMessageText()}" /></p>
              </div>
            </div>
          </div>
          <div class="like">
            <a href="/post/${post.getId()}#comments"><fmt:message key="label.comments"/><c:out value="${post.getCommentNumber()}"/> </a>
          </div>
        </div>
      </c:forEach>
    </section>

    <div class="col-md-3">

      <!-- Some news -->
      <aside class="well">
        <h4><fmt:message key="label.recent.news.header"/></h4>
        <p><fmt:message key="label.recent.news.body"/></p>
      </aside>
      <aside>
          <a href="/createpost"><button class="btn btn-large btn-block question-btn" type="submit"><fmt:message key="label.button.ask.qustions"/></button></a>
      </aside>

    </div>
  </div>
</div>
<!-- Footer -->
<footer>
  <p>Copyright &copy; Murdeus Website 2016</p>
</footer>

</body>
</html>

