<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="com.epam.courses.forum.resources.text" />

<!DOCTYPE html>
<html lang="${language}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Sign In page for LikeIt Forum">
  <meta name="author" content="Murdeus">

  <title>LikeIt - <fmt:message key="label.sign.in"/></title>

  <!-- Bootstrap core CSS -->
  <link href="/css/bootstrap.css" rel="stylesheet">

  <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
  <link href="/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="/css/custom.css" rel="stylesheet">
  <link href="/css/common.css" rel="stylesheet">
  <link href="/css/signin.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body>

<div class="container">
  <form name="loginForm" action="login" method="post" class="form-signin">
    <input type="hidden" name="command" value="login">

    <h2 class="form-signin-heading"><fmt:message key="login.label.signin.message" /></h2>
    <label for="inputUsername" class="sr-only">  <fmt:message key="login.label.username" />  </label>
    <input id="inputUsername" name="inputUsername" class="form-control" placeholder="<fmt:message key="login.label.username" />" required autofocus>
    <label for="inputPassword" class="sr-only">  <fmt:message key="login.label.password" />   </label>
    <input type="password" id="inputPassword" pattern=".{8,32}" name="inputPassword" class="form-control" placeholder="<fmt:message key="login.label.password" />" required>
    <div class="pass-forget">
    ${errorLoginBannedMessage}
    ${errorLoginPassMessage}
    </div>
    <button class="btn btn-lg btn-block btn-edit btn-submit" name="loginBtn" value="Log in" type="submit"><fmt:message key="label.sign.in" /></button>
    <div class="pass-forget">
      <fmt:message key="login.label.no.account"/> <a href="/signup"><fmt:message key="label.sign.up"/></a>
    </div>
  </form>
</div>

<footer>
  <p>Copyright &copy; Murdeus Website 2016</p>
</footer>
</body>
</html>

