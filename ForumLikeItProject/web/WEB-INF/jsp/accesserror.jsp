<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="com.epam.courses.forum.resources.text" />
<!DOCTYPE html>
<html lang="${language}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Sign In page for LikeIt Forum">
  <meta name="author" content="Murdeus">

  <title>LikeIt - <fmt:message key="label.access.denied"/></title>

  <!-- Bootstrap core CSS -->
  <link href="/css/bootstrap.css" rel="stylesheet">

  <link href="/css/custom.css" rel="stylesheet">

  <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
  <link href="/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="/css/custom.css" rel="stylesheet">
  <link href="/css/common.css" rel="stylesheet">
  <link href="/css/signin.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body>
<div class="container">
  <form class="form-error">
    <h1 class="form-signin-heading"><fmt:message key="label.access.denied"/></h1>
    <div class="pass-forget">
      <a href="/index"><fmt:message key="label.to.main.page"/></a>
    </div>
  </form>
</div>

<footer>
  <p>Copyright &copy; Murdeus Website 2016</p>
</footer>
</body>
</html>
