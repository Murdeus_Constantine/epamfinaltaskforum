<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="cy" uri="customtags" %>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="com.epam.courses.forum.resources.text" />

<!DOCTYPE html>
<html lang="${language}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Post create page of LikeIt Forum">
  <meta name="author" content="Murdeus">

  <title>LikeIt - <fmt:message key="label.edit.question"/></title>

  <!-- Bootstrap Core CSS -->
  <link href="/css/bootstrap.css" rel="stylesheet">

  <!-- Custom CSS -->
  <link href="/css/custom.css" rel="stylesheet">
  <link href="/css/common.css" rel="stylesheet">
  <link href="/css/create.css" rel="stylesheet">

  <!-- jQuery -->
  <script src="/js/jquery.js"></script>

  <!-- Bootstrap Core JavaScript -->
  <script src="/js/bootstrap.min.js"></script>

  <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
  <link href="/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body>
<nav class="navbar navbar-inverse navbar-fixed-top" >
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/index"><fmt:message key="label.home"/></a>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav ">
        <cy:registered-only>
          <li>
            <a href="/myposts"><fmt:message key="label.my.post"/></a>
          </li>
        </cy:registered-only>
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown"><fmt:message key="label.language"/>
            <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="/switchLocale?lang=en">English</a></li>
            <li><a href="/switchLocale?lang=ru">Русский</a></li>
          </ul>
        </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <c:if test="${'admin' == sessionScope.userRole}">
          <li>
            <a href="/admin">
              <span class="glyphicon glyphicon-tower"></span><fmt:message key="label.admin.page"/></a>
          </li>
        </c:if>
        <c:choose>
          <c:when test="${sessionScope.userId == null}">
            <li>
              <a href="/signup"><fmt:message key="label.sign.up"/></a>
            </li>
            <li>
              <a href="/signin">
                <span class="glyphicon glyphicon-log-in"></span> <fmt:message key="label.sign.in"/></a>
            </li>
          </c:when>
          <c:otherwise>
            <li>
              <a href="/user/${sessionScope.userId}"><span class="glyphicon glyphicon-user"></span> <fmt:message key="label.profile.page"/></a>
            </li>
            <li>
              <a href="/logout">
                <span class="glyphicon glyphicon-log-out" ></span><fmt:message key="label.log.out"/></a>
            </li>
          </c:otherwise>
        </c:choose>
      </ul>
    </div>
  </div>
</nav>

<div class="container">
  <div class="row">
    <section class="col-lg-9">
      <div class="post-create well">
        <h3><fmt:message key="label.edit.question"/></h3>
        <hr>
        <form action="postedit" method="post">
          <input type="hidden" name="command" value="postedit">
          <input type="hidden" name="changePostId" value=${post.getId()}>
          <dl>
            <dt><label for="inputPostTopicEdit"><fmt:message key="label.post.topic"/></label></dt>
            <dd><textarea maxlength="128" required id="inputPostTopicEdit" name="inputPostTopicEdit" class="form-control"><c:out value="${post.messageTopic}" /></textarea></dd>
            <dt><label for="inputPostBodyEdit"><fmt:message key="label.post.body"/></label></dt>
            <dd><textarea maxlength="512" required id="inputPostBodyEdit" name="inputPostBodyEdit" class="form-control post-body"><c:out value="${post.messageText}" /></textarea></dd>
            ${errorPostEditMessage}
          </dl>
          <button class="btn btn-lg btn-edit btn-sign-up" type="submit"><fmt:message key="label.button.question.edit"/></button>
        </form>
      </div>
    </section>

    <div class="col-md-3">

      <!-- Some news -->
      <aside class="well">
        <h4><fmt:message key="label.recent.news.header"/></h4>
        <p><fmt:message key="label.recent.news.body"/></p>
      </aside>
      <aside>
        <a href="/createpost"><button class="btn btn-large btn-block question-btn" type="submit"><fmt:message key="label.button.ask.qustions"/></button></a>
      </aside>

    </div>
  </div>
</div>

<footer>
  <p>Copyright &copy; Murdeus Website 2016</p>
</footer>
</body>
</html>
